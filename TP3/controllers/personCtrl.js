const db = require('../models');

module.exports = {

  //renvoie l'ensemble des personnes sous la forme d'un tableau d'objets en JSON
  get_all: (req, res) => {
    //fonction asynchrone qui va choper toutes les infos de la BBD dans l'ordre de lastname
    return db.Person.findAll({
			order: [ 'lastname' ]
		}) 
    .then((p)=>{ //tout va être enregistré dans le tableau p, renvoyé dans une promise
      res.json(p); //ce tableau je veux le transformer en json et en faire une requête http
    })
  },

  //ajoute une nouvelle entité `person` en base à partir des données
  create: (req, res) => {
    //on créé un objet constitué des données dans la requête
    return db.Person.create({
        firstname: req.body.Firstname,
        lastname: req.body.Lastname
    //promise pour envoyer le résultat
    })
    .then(result =>
        res.send(result))
  },
  
  /*renvoi la personne en fonction de l'id en param*/
  get_by_id: (req, res) => {
    //on créé une variable de bloc contenant un tableau vide
    let include = [];
    //si la requête contient un paramètre URL include (*url*?include=x)
		if (req.query.include) {
      //si il le paramètre d'URL include renvoi plusieurs valeurs, donc sous forme de tableau ?
			if (Array.isArray(req.query.include)) {
        //on traite chaque valeur jusqu'à trouver la valeur "MailAddress", puis on ajoute l'ensemble des objet "mailaddress" dans la variable include précédemment créé
				req.query.include.forEach((i) => {
					if (i === 'MailAddress') { include.push(MailAddress); }
        });
        //sinon si include n'a qu'une seule valeur, pas besoin de faire une boucle, on fait un test directement sur la seule valeur
			} else {
				if (req.query.include === 'MailAddress') {
					include.push(MailAddress);
				}
			}
    }
    //on renvoi les objets "mailaddress" appartenant à l'objet dont l'id correspond à l'attribut id en url
		return db.Person.findById(req.params.id, {
			include
		})
    .then(persons => {
      res.send(persons);
    })
  },

  //modifie la personne d'identifiant id` à partir des données fournies dans la requête
  update_by_id: (req, res) => {
    //on va modifier en bdd les variables suivants avec les données contenu dans le body de la requête
    db.Person.update({
      firstname: req.body.Firstname,
      lastname: req.body.Lastname
    }, {
      returning: true,
      //on modifiera si l'id en base de données est égal à celui de la variable id de la requête
      where: {
          id: req.params.id
      }
    })
    //réponses
    .then(
      console.log('Person modified : \nID:' + req.params.id + '\nPrénom : ' + req.body.Firstname + '\nNom : ' + req.body.Lastname)
    )
    .returning(
      res.send('Person modified : \nID:' + req.params.id + '\nPrénom : ' + req.body.Firstname + '\nNom : ' + req.body.Lastname)
    )
    .error(
      console.log('The ID ' + req.params.id + 'does not exists...')
    )
    .catch(
      console.log('The ID ' + req.params.id + 'does not exists...')
    )
  },

  //supprime la personne d'identifiant id de la base de données
  delete_by_id: (req, res)=>{
    db.Person.destroy({
      //on supprimera si l'id en base de données est égal à celui de la variable id de la requête
      where: {
          id: req.params.id
      }
    })
    //réponse
    .then(
      res.send('The ID ' + req.params.id + ' has been deleted...')
    )
  },

  //
  load_by_id: (req, res, next) => {
    //on cherche l'objet en base de données en fonction de l'id de ce denrier passé dans l'url
		return db.Person.findById(req.params.id)
		.then((person) => {
      //si il n'existe pas on envoi érreur
			if (!person) {
				throw { status: 404, message: 'Requested Person not found' };
      }
      //on charge dans la requête l'objet "person" qu'on a obtenun de la base de données
      req.person = person;
      //puis appel la fonction middleware suivant dans la pile
			return next();
		})
		.catch((err) => next(err));
	},

};