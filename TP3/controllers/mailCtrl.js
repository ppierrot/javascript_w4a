const db = require('../models');

module.exports = {

    //ajoute une nouvelle entité `MailAddress` en base à partir des données
    create: (req, res) => {
        //on créé une variable constitué des données dans la requête
        const data = {
            address: req.body.Address,
            type: req.body.Type,
            PersonId: req.params.id
        };
        //on retourne la création d'un mail selon l'id de l'objet 'person' indiqué dans l'url
        return db.Mail.create(data)
        //promise pour convertir le résultat en json
        .then((person) => res.json(person))
    },

    //récupère tous les objets 'MailAddress' en base
    get_all: (req, res, next) => {
        //tel un findall(), on va chercher avec une requête get tous les ojets 'mailaddress", ils seront affiché dans l'ordre de l'attribut "type"
		db.Mail.findAll()
		.then((mailAddresses) => res.json(mailAddresses))
		.catch((err) => next(err));
    },
    
    //récupère un objet 'mailaddress' en base selon l'id dans l'url
    get_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'mailaddress'
		db.Mail.findById(req.params.mail_address_id)
		.then((mailAddresses) => {
            //si l'objet récupéré ne contient aucun caractère alors erreur
			if (!mailAddresses) {
				throw { status: 404, message: 'Requested MailAddress not found' };
            }
            //on retourne un json contenant le premier index du tableau mailaddress
			return res.json(mailAddresses);
		})
		.catch((err) => next(err));
    },

    //on va modifier un objet 'maidaddress' par un autre contenu dans l'url
    update_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'mailaddress'
		return db.Mail.findById(req.params.mail_address_id)
		.then((mailAddresses) => {
            //si l'objet récupéré ne contient aucun caractère alors erreur
			if (!mailAddresses) {
				throw { status: 404, message: 'Requested MailAddress not found' };
            }
            return mailAddresses.update(req.body);
		})
        .then((mailAddresses) => res.json(mailAddresses))
		.catch((err) => next(err));
    },
    
    //on va effacer un objet 'mailaddress' qu'on identifie dans l'url
    delete_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'mailaddress'
        return db.Mail.findById(req.params.mail_address_id)
		.then((mailAddresses) => {
            //si l'objet récupéré ne contient aucun caractère alors erreur
			if (mailAddresses.length === 0) {
				throw { status: 404, message: 'Requested MailAddress not found' };
            }
            //on détruit le premier index du tableau mailaddress en bdd
			return mailAddresses.destroy();
		})
        //renvoyer un statut 200 signifie que l'opération s'est bien déroulé
		.then(() => res.status(200).end())
		.catch((err) => next(err));
	},
};