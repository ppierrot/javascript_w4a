const mailCtrl = require('../controllers/mailCtrl');

module.exports = [
	
	{
		url: '/person/:id/mailAddress',
		method: 'get',
		func: mailCtrl.get_all
    },

    {
		url: '/person/:id/mailAddress',
		method: 'post',
		func: mailCtrl.create
    },

    {
		url: '/person/:id/mailAddress/:mail_address_id',
		method: 'get',
		func: mailCtrl.get_by_id
    },

    {
		url: '/person/:id/mailAddress/:mail_address_id',
		method: 'put',
		func: mailCtrl.update_by_id
    },

    {
		url: '/person/:id/mailAddress/:mail_address_id',
		method: 'delete',
		func: mailCtrl.delete_by_id
    }
]