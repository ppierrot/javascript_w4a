const db = require('../models');

module.exports = {
  
  //pour faire des get[nom_de_table], il faut que les relations entre modèles soit bonne : belongsToMany

  //renvoie l'ensemble des groupes de la personne d'identifiant `person_id` sous la forme d'un tableau d'objets en JSON
  get_all: (req, res) => {
    return req.person.getGroups()
    .then((array) => res.json(array))
  },

  //renvoie l'ensemble des personnes du groupe d'identifiant `group_id` sous la forme d'un tableau d'objets en JSON
  get_group_persons: (req,res) =>{
    return req.group.getPeople()
    .then((array) => res.json(array))
  },

  //ajoute la personne d'identifiant `id` au groupe d'identifiant 'group_id'
  add_person_to_group: (req,res)=>{
    return req.group.addPeople(req.person)
    .then(res.send('Ajout reussi'));
  },

  //supprime la personne d'identifiant `id` au groupe d'identifiant 'group_id'
  delete_person_from_group: (req,res)=>{
    return req.group.removePeople(req.person)
    .then(res.send('Suppression reussi'));
  },

  //ajoute le groupe d'identifiant `group_id` à la personne d'identifiant `person_id`
  add_group_to_person: (req,res)=>{
    return req.person.addGroups(req.group)
    .then(res.send('Ajout reussi'));
  },

  //supprime le groupe d'identifiant `group_id` à la personne d'identifiant `person_id`
  delete_group_from_person: (req,res)=>{
    return req.person.removeGroups(req.group)
    .then(res.send('Suppression reussi'));
  }

}