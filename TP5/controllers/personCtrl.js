const db = require('../models');

module.exports = {

  //renvoie l'ensemble des personnes sous la forme d'un tableau d'objets en JSON
  get_all: (req, res) => {
    //si l'attribut lastname dans la query string exite
    if(req.query.lastname != null){
    //on récupère tous les person dont le lastname est égal a l'attribut lastname dans la query string  
      data = db.Person.findAll({where: {lastname: req.query.lastname}})
    }
    //sinon on cherche tout regardless
		else{
			data = db.Person.findAll({
        order: [ 'lastname' ]
      }) 
    }
    return data
    .then((p)=>{ //tout va être enregistré dans le tableau p, renvoyé dans une promise
      res.json(p); //ce tableau je veux le transformer en json et en faire une requête http
    })
  },

  //ajoute une nouvelle entité `person` en base à partir des données
  create: (req, res) => {
    //on créé un objet constitué des données dans la requête
    return db.Person.create({
        firstname: req.body.firstname,
        lastname: req.body.lastname
    //promise pour envoyer le résultat
    })
    .then(result =>
        res.send(result))
  },
  
  /*renvoi la personne en fonction de l'id en param*/
  get_by_id: (req, res) => {
    //on renvoi les objets "person" appartenant à l'objet dont l'id correspond à la variable id en url
		return db.Person.findById(req.params.id)
    .then(persons => {
      res.send(persons);
    })
  },

  //modifie la personne d'identifiant id` à partir des données fournies dans la requête
  update_by_id: (req, res) => {
    //on va modifier en bdd les variables suivants avec les données contenu dans le body de la requête
    db.Person.update({
      firstname: req.body.firstname,
      lastname: req.body.lastname
    }, {
      returning: true,
      //on modifiera si l'id en base de données est égal à celui de la variable id de la requête
      where: {
          id: req.params.id
      }
    })
    //réponses
    .then(
      console.log('Person modified : \nID:' + req.params.id + '\nPrénom : ' + req.body.firstname + '\nNom : ' + req.body.lastname)
    )
    .returning(
      res.send('Person modified : \nID:' + req.params.id + '\nPrénom : ' + req.body.firstname + '\nNom : ' + req.body.lastname)
    )
    .error(
      console.log('The ID ' + req.params.id + 'does not exists...')
    )
    .catch(
      console.log('The ID ' + req.params.id + 'does not exists...')
    )
  },

  //supprime la personne d'identifiant id de la base de données
  delete_by_id: (req, res)=>{
    db.Person.destroy({
      //on supprimera si l'id en base de données est égal à celui de la variable id de la requête
      where: {
          id: req.params.id
      }
    })
    //réponse
    .then(
      res.send('The ID ' + req.params.id + ' has been deleted...')
    )
  },

  //cherche la person d'identifiant id de l'url puis l'envoi via la requête au prochain middleware
  load_by_id: (req, res, next) => {
    //on cherche l'objet en base de données en fonction de l'id de ce denrier passé dans l'url
		return db.Person.findById(req.params.id)
		.then((person) => {
      //si il n'existe pas on envoi érreur
			if (!person) {
				throw { status: 404, message: 'Requested Person not found' };
      }
      //on charge dans la requête l'objet "person" qu'on a obtenu, dans la requête
      req.person = person;
      //puis appel la fonction middleware suivant dans la pile, on lui envoi les informations
			return next();
		})
		.catch((err) => next(err));
	},

};