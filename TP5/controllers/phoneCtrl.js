const db = require('../models');

module.exports = {

    //ajoute une nouvelle entité `Phone` en base à partir des données
    create: (req, res) => {
        //on créé une variable constitué des données dans la requête
        const data = {
            address: req.body.address,
            type: req.body.type,
            PersonId: req.params.id
        };
        //on retourne la création d'un phone selon l'id de l'objet 'person' indiqué dans l'url
        return db.Phone.create(data)
        //promise pour convertir le résultat en json
        .then((person) => res.json(person))
    },

    //récupère tous les objets 'phone' en base
    get_all: (req, res, next) => {
        //tel un findall(), on va chercher avec une requête get tous les ojets "phone", ils seront affiché dans l'ordre de l'attribut "type"
        if(req.query.type != null){
            //on récupère tous les mails dont l'id Person est celui de la variable id contenu dans l'url et le type contenu dans l'attribut type dans la query string  
			data = db.Phone.findAll({where: {PersonId: req.params.id} && {type: req.query.type}})
        }
        //sinon on ne fiat que chercher en fonction du contenu de la variable id pour Person
		else{
			data = db.Phone.findAll({where: {PersonId: req.params.id}})
		}
		return data
		.then((phoneNumber) => res.json(phoneNumber))
		.catch((err) => next(err));
    },
    
    //récupère un objet 'phone' en base selon l'id dans l'url
    get_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'phone'
		db.Phone.findById(req.params.phone_number_id)
		.then((phoneNumber) => {
            //si l'objet récupéré ne contient aucun caractère alors erreur
			if (!phoneNumber) {
				throw { status: 404, message: 'Requested phoneNumber not found' };
            }
            //on retourne un json 
			return res.json(phoneNumber);
		})
		.catch((err) => next(err));
    },

    //on va modifier un objet 'phone' par un autre contenu dans l'url
    update_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'phone'
		return db.Phone.findById(req.params.phone_number_id)
		.then((phoneNumber) => {
            //si l'objet récupéré ne contient aucun caractère alors erreur
			if (!phoneNumber) {
				throw { status: 404, message: 'Requested phoneNumber not found' };
            }
            return phoneNumber.update(req.body);
		})
        .then((phoneNumber) => res.json(phoneNumber))
		.catch((err) => next(err));
    },
    
    //on va effacer un objet 'phone' qu'on identifie dans l'url
    delete_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'phone'
        return db.Phone.findById(req.params.phone_number_id)
		.then((phoneNumber) => {
            //si l'objet récupéré n'existe pas
			if (!phoneNumber) {
				throw { status: 404, message: 'Requested phoneNumber not found' };
            }
            //on détruit le premier index du tableau post en bdd
			return phoneNumber.destroy();
		})
        //renvoyer un statut 200 signifie que l'opération s'est bien déroulé
		.then(() => res.status(200).end())
		.catch((err) => next(err));
	},
};