const db = require('../models');

module.exports = {

    //renvoie l'ensemble des groupes existants
    get_all: (req, res) => {
            db.Group.findAll({order: [ 'title' ]
        })
        .then((p)=>{ //tout va être enregistré dans le tableau p, renvoyé dans une promise
        res.json(p); //ce tableau je veux le transformer en json et en faire une requête http      
        })
    },

    //créé un groupe avec un champs "tile" dans le corps de la requête
    create: (req, res) => {
        return db.Group.create({
            title: req.body.title
        //promise pour envoyer le résultat
        })
        .then(result =>
            res.send(result))
    },

    //cherche un group par son identifiant
    get_by_id: (req, res) => {
        return db.Group.findById(req.params.group_id)
        .then(groups => {
        res.send(groups);
        })
    },

    //modifie le title d'un groupe avec le champs "tile" dans le corps de la requête
    update_by_id: (req, res) => {
        db.Group.update({
            title: req.body.title
          }, {
            returning: true,
            //on modifiera si l'id en base de données est égal à celui de la variable id de l'url
            where: {
                id: req.params.group_id
            }
          })
          //réponses
          .then(
            console.log('Group modified : \nID:' + req.params.group_id + '\nTitle : ' + req.body.title)
          )
          .returning(
            res.send('Group modified : \nID:' + req.params.group_id + '\nTitle : ' + req.body.title)
          )
          .error(
            console.log('The ID ' + req.params.group_id + 'does not exists...')
          )
          .catch(
            console.log('The ID ' + req.params.group_id + 'does not exists...')
          )
    },

    //supprime un groupe
    delete_by_id: (req, res)=>{
        db.Group.destroy({
            //on supprimera si l'id en base de données est égal à celui de la variable id de l'url
            where: {
                id: req.params.group_id
            }
          })
          //réponse
          .then(
            res.send('The ID ' + req.params.group_id + ' has been deleted...')
          )
    },

    //cherche le group d'identifiant id de l'url puis l'envoi via la requête au prochain middleware
    load_group: (req, res, next) => {
      //cherche par id dans la bdd
      return db.Group.findById(req.params.group_id)
      //si on a rien trouvé : error 404 sinon
      .then((g) => {
        if (!g) {
          throw {
            status: 404,
            message: 'Group not found'
          }};
        //on enregistre ce qu'on a trouvé dans la promise
        req.group = g;
        next();
      })
      //on renvoi la promise et son contneu vers le prochain middleware a l'aide de next
      .catch((err) => next(err));
    },
  
}