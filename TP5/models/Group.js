//`DataTypes` qui contient l'ensemble des types de données que l'on peut utiliser pour définir les champs des entités
module.exports = function (sequelize, DataTypes) {

	const Group = sequelize.define(
		'Group',
		{
			title:  DataTypes.STRING
		}
	);
    //Cette fonction doit faire partie des méthodes du modèle, elle reçoit l'objet `db` en paramètre et déclare d'éventuelles relations entre ce modèle et d'autres modèles définis dans `db`.
	Group.associate = (db) => {
		//Many-to-many association with a join table
		Group.belongsToMany(db.Person, { 
            through: {
                model: "GroupPerson"
            } 
        });
	};
	
	return Group;

};