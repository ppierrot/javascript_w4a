const personCtrl = require('../controllers/personCtrl');

module.exports = [
	
	{
		url: '/person',
		method: 'get',
		func: personCtrl.get_all
    },
    
    {
		url: '/person',
		method: 'post',
		func: personCtrl.create
    },
    
    {
		url: '/person/:id',
		method: 'get',
		func: personCtrl.get_by_id
    },

    {
		url: '/person/:id',
		method: 'put',
		func: personCtrl.update_by_id
    },

    {
		url: '/person/:id',
		method: 'delete',
		func: personCtrl.delete_by_id
    }
];