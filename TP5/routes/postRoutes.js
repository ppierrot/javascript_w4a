const postCtrl = require('../controllers/postCtrl');
const personCtrl = require('../controllers/personCtrl');

module.exports = [
	
	{
		url: '/person/:id/postAddress',
		method: 'get',
		func: postCtrl.get_all
    },

    {
		url: '/person/:id/postAddress',
		method: 'post',
		func: postCtrl.create
    },

    {
		url: '/person/:id/postAddress/:post_address_id',
		method: 'get',
		func: [personCtrl.load_by_id, postCtrl.get_by_id]
    },

    {
		url: '/person/:id/postAddress/:post_address_id',
		method: 'put',
		func: [personCtrl.load_by_id, postCtrl.update_by_id]
    },

    {
		url: '/person/:id/postAddress/:post_address_id',
		method: 'delete',
		func: [personCtrl.load_by_id, postCtrl.delete_by_id]
    }
]