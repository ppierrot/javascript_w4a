const groupCtrl = require('../controllers/groupCtrl');

module.exports = [
		
		//cherche tous les group existants
    {
		url: '/group',
		method: 'get',
		func: groupCtrl.get_all
    },
		
		//créé un nouveau group
    {
		url: '/group',
		method: 'post',
		func: groupCtrl.create
    },

		//cherche un group via :group_id
    {
		url: '/group/:group_id',
		method: 'get',
		func: groupCtrl.get_by_id
    },

		//modifie l'enregistrement d'id :group_id
    {
		url: '/group/:group_id',
		method: 'put',
		func: groupCtrl.update_by_id
    },

		//détruit l'enregistrement d'id :group_id
    {
		url: '/group/:group_id',
		method: 'delete',
		func: groupCtrl.delete_by_id
		}
	
];