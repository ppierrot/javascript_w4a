const phoneCtrl = require('../controllers/phoneCtrl');
const personCtrl = require('../controllers/personCtrl');

module.exports = [
	
	{
		url: '/person/:id/phoneNumber',
		method: 'get',
		func: phoneCtrl.get_all
    },

    {
		url: '/person/:id/phoneNumber',
		method: 'post',
		func: phoneCtrl.create
    },

    {
		url: '/person/:id/phoneNumber/:phone_number_id',
		method: 'get',
		func: [personCtrl.load_by_id, phoneCtrl.get_by_id]
    },

    {
		url: '/person/:id/phoneNumber/:phone_number_id',
		method: 'put',
		func: [personCtrl.load_by_id, phoneCtrl.update_by_id]
    },

    {
		url: '/person/:id/phoneNumber/:phone_number_id',
		method: 'delete',
		func: [personCtrl.load_by_id, phoneCtrl.delete_by_id]
    }
]