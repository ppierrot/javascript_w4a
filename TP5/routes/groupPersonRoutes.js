const groupPersonCtrl = require('../controllers/groupPersonCtrl');
const groupCtrl = require('../controllers/groupCtrl');
const personCtrl = require('../controllers/personCtrl');

module.exports = [

  //chercher tous les group auquel appartient la person :id
	{
		url: '/person/:id/group',
		method: 'get',
		func: [personCtrl.load_by_id, groupPersonCtrl.get_all]
	},
  
  //chercher tous les person contenu dans le groupe :group_id
  {
    url: '/group/:group_id/person',
    method: 'get',
    func: [groupCtrl.load_group, groupPersonCtrl.get_group_persons]
	},
  
  //on intégre la person :id dans le groupe :group_id
  {
    url: '/person/:id/group/:group_id',
    method: 'post',
    func: [groupCtrl.load_group, personCtrl.load_by_id, groupPersonCtrl.add_person_to_group]
  },
    
  //on supprime la person :id du groupe :group_id
	{
    url: '/person/:id/group/:group_id',
    method: 'delete',
    func: [groupCtrl.load_group, personCtrl.load_by_id, groupPersonCtrl.delete_person_from_group]
	},
   
  //on assigne le :group_id à la person :id
  {
    url: '/group/:group_id/person/:id',
    method: 'post',
    func: [groupCtrl.load_group, personCtrl.load_by_id, groupPersonCtrl.add_group_to_person]
	},
  
  //on supprime le: group_id du repertoire de group de la person :id
  {
    url: '/group/:group_id/person/:id',
    method: 'delete',
    func: [groupCtrl.load_group, personCtrl.load_by_id, groupPersonCtrl.delete_group_from_person]
  },

];