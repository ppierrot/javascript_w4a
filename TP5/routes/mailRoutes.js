const mailCtrl = require('../controllers/mailCtrl');
const personCtrl = require('../controllers/personCtrl');

module.exports = [
	
	{
		url: '/person/:id/mailAddress',
		method: 'get',
		func: mailCtrl.get_all
    },

    {
		url: '/person/:id/mailAddress',
		method: 'post',
		func: mailCtrl.create
    },

		//dans func, le premier paramètre est un middleware pour charger les objets person
    {
		url: '/person/:id/mailAddress/:mail_address_id',
		method: 'get',
		func: [personCtrl.load_by_id, mailCtrl.get_by_id]
    },

    {
		url: '/person/:id/mailAddress/:mail_address_id',
		method: 'put',
		func: [personCtrl.load_by_id, mailCtrl.update_by_id]
    },

    {
		url: '/person/:id/mailAddress/:mail_address_id',
		method: 'delete',
		func: [personCtrl.load_by_id, mailCtrl.delete_by_id]
    }
]