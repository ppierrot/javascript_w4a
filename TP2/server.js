//on appel body parser pour décrypter les données qu'on reçoit dans une requête
const bodyParser = require('body-parser');
//on déclare la libraire express et on le met dans la variable app 
const express = require('express')
const app = express(); //framework pour gérer le routage des liens URL

// fonctions pour faire appel aux contrôleurs de chaque modèles contenus dans d'autres fichiers
const personCtrl = require('./personCtrl');
const mailCtrl = require('./mailCtrl');

//pour automatiquement parser les données et les convertir en json quand on utilise app
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Gestion d'erreurs (Middleware)
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

//Routes person
app.get('/', (req, res) => res.send('Hello World!'))
app.get('/person', personCtrl.get_all);
app.post('/person', personCtrl.create);
app.get('/person/:id', personCtrl.get_by_id);
app.put('/person/:id', personCtrl.update_by_id);
app.delete('/person/:id', personCtrl.delete_by_id);

//Routes mail
app.get('/person/:id/mailAddress', personCtrl.load_by_id, mailCtrl.get_all);
app.post('/person/:id/mailAddress', personCtrl.load_by_id, mailCtrl.create);
app.get('/person/:id/mailAddress/:mail_address_id', personCtrl.load_by_id, mailCtrl.get_by_id);
app.put('/person/:id/mailAddress/:mail_address_id', personCtrl.load_by_id, mailCtrl.update_by_id);
app.delete('/person/:id/mailAddress/:mail_address_id', personCtrl.load_by_id, mailCtrl.delete_by_id);

//si on est connécté avec succès sur le port 3000, affiche le log suivant
app.listen(3000, () => console.log('Example app listening on port 3000!'))