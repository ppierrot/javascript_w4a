//on appel la libraire sequelize qui est l'ORM, il sevira de lien vers la bdd sans faire de requêtes sql dans le code mais en passant par des modèles
const Sequelize = require('sequelize');

//on connecte sequelize à notre base de donnée : nom bbd, user, password
const sequelize = new Sequelize('apiweb', 'apiweb', 'apiweb', {
    host: 'localhost',
    dialect: 'mysql',
  
    // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
    operatorsAliases: false
});

//===========
//modèles
//===========

//on déclare une variable Person contenant un modèle (soit un tableau de bdd) de nom Person avec une colonne firstname et lastname
const Person = sequelize.define('Person', {
    firstname: Sequelize.STRING,//colonne de la table
    lastname: Sequelize.STRING//colonne de la table
});
  
//de même pour le modèle mail
const Mail = sequelize.define('MailAddress', {
    address: {
      type: Sequelize.STRING,
      //on vérifie si c'est bien le format d'un mail XXX@XXX.XXX
      validate: {
        isEmail: true
      }
    },
    //type est ou bien 'home' ou 'work'
    type: Sequelize.ENUM('home', 'work')
});

// un objet Person peut posséder plusieurs objets Mail, si Person est supprimer alors ses Mail le sont aussi
Person.hasMany(Mail, { onDelete: 'cascade' });
// un objet Mail appartient à un objet Person 
Mail.belongsTo(Person, { onDelete: 'cascade' });

//synchronisation avec la base de données
sequelize.sync();

module.exports = {
	Person,
	Mail
};