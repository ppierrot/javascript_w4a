# W4a

installation :
* dl et install node : https://nodejs.org/en/download/
* une fois installé, on démarre un terminal (git bash ou visual studio code) et on entre "node"
* on vérifie version avec “npm” puis “npm init” pour installer un nouveau projet
* on va à la racine du projet puis on fait “npm install -save express...” faire pareil pour parse et sequelize
penser au .gitignore si gitlab est utilisé

(!) quand on prend la date en bdd, ne pas intégrer l'heure pour éviter les décalages horaires
(!) utilise, voir rubrique “model definitions-validations” dans doc sequelize : moyens de valider la valeur d'un champ pour être sur que les données sont cohérentes avec les contraintes de validations

utilisation :
* démarrer Uwamp ou autre serveur HTTP genre Apache
* démarer terminal (git bash ou visual studio code) à la racine du projet
* pour démarrer serveur : node [index.js/server.js]
* tester les requêtes avec Postman
