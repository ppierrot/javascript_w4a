const phoneCtrl = require('../controllers/phoneCtrl');

module.exports = [
	
	{
		url: '/person/:id/phoneNumber',
		method: 'get',
		func: phoneCtrl.get_all
    },

    {
		url: '/person/:id/phoneNumber',
		method: 'post',
		func: phoneCtrl.create
    },

    {
		url: '/person/:id/phoneNumber/:phone_number_id',
		method: 'get',
		func: phoneCtrl.get_by_id
    },

    {
		url: '/person/:id/phoneNumber/:phone_number_id',
		method: 'put',
		func: phoneCtrl.update_by_id
    },

    {
		url: '/person/:id/phoneNumber/:phone_number_id',
		method: 'delete',
		func: phoneCtrl.delete_by_id
    }
]