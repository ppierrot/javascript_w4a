const postCtrl = require('../controllers/postCtrl');

module.exports = [
	
	{
		url: '/person/:id/postAddress',
		method: 'get',
		func: postCtrl.get_all
    },

    {
		url: '/person/:id/postAddress',
		method: 'post',
		func: postCtrl.create
    },

    {
		url: '/person/:id/postAddress/:post_address_id',
		method: 'get',
		func: postCtrl.get_by_id
    },

    {
		url: '/person/:id/postAddress/:post_address_id',
		method: 'put',
		func: postCtrl.update_by_id
    },

    {
		url: '/person/:id/postAddress/:post_address_id',
		method: 'delete',
		func: postCtrl.delete_by_id
    }
]