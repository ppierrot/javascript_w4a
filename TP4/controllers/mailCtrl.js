const db = require('../models');

module.exports = {

    //ajoute une nouvelle entité `MailAddress` en base à partir des données
    create: (req, res) => {
        //on créé une variable constitué des données dans la requête
        const data = {
            address: req.body.Address,
            type: req.body.Type,
            PersonId: req.params.id
        };
        //on retourne la création d'un mail selon l'id de l'objet 'person' indiqué dans l'url
        return db.Mail.create(data)
        //promise pour convertir le résultat en json
        .then((person) => res.json(person))
    },

    //récupère tous les objets 'MailAddress' en base
    get_all: (req, res, next) => {
        //si l'attribut `type` existe dans la query string des routes 
        if(req.query.type != null){
            //on récupère tous les mails dont l'id Person est celui de la variable id contenu dans l'url et le type contenu dans l'attribut type dans la query string  
			data = db.Mail.findAll({where: {PersonId: req.params.id} && {type: req.query.type}})
        }
        //sinon on ne fiat que chercher en fonction du contenu de la variable id pour Person
		else{
			data = db.Mail.findAll({where: {PersonId: req.params.id}})
		}
		return data
		.then((mailAddresses) => res.json(mailAddresses))
		.catch((err) => next(err));
    },
    
    //récupère un objet 'mailaddress' en base selon l'id dans l'url
    get_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'mailaddress'
		db.Mail.findById(req.params.mail_address_id)
		.then((mailAddresses) => {
            //si l'objet récupéré ne contient aucun caractère alors erreur
			if (!mailAddresses) {
				throw { status: 404, message: 'Requested MailAddress not found' };
            }
            //on retourne un json
			return res.json(mailAddresses);
		})
		.catch((err) => next(err));
    },

    //on va modifier un objet 'maidaddress' par un autre contenu dans l'url
    update_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'mailaddress'
		return db.Mail.findById(req.params.mail_address_id)
		.then((mailAddresses) => {
            //si l'objet récupéré ne contient aucun caractère alors erreur
			if (!mailAddresses) {
				throw { status: 404, message: 'Requested MailAddress not found' };
            }
            return mailAddresses.update(req.body);
		})
        .then((mailAddresses) => res.json(mailAddresses))
		.catch((err) => next(err));
    },
    
    //on va effacer un objet 'mailaddress' qu'on identifie dans l'url
    delete_by_id: (req, res, next) => {
        //requête get dans la bdd pour chercher un objet 'mailaddress'
        return db.Mail.findById(req.params.mail_address_id)
		.then((mailAddresses) => {
            //si l'objet récupéré n'existe pas
			if (!mailAddresses) {
				throw { status: 404, message: 'Requested MailAddress not found' };
            }
            //on détruit le premier index du tableau mailaddress en bdd
			return mailAddresses.destroy();
		})
        //renvoyer un statut 200 signifie que l'opération s'est bien déroulé
		.then(() => res.status(200).end())
		.catch((err) => next(err));
	},
};