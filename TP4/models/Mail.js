//`DataTypes` qui contient l'ensemble des types de données que l'on peut utiliser pour définir les champs des entités
module.exports = function (sequelize, DataTypes) {

	const Mail = sequelize.define(
		'Mail',
		{
			address: {
                type: DataTypes.STRING,
                //on vérifie si c'est bien le format d'un mail XXX@XXX.XXX
                validate: {
                  isEmail: true
                }
              },
            //type est ou bien 'home' ou 'work'
            type: DataTypes.ENUM('home', 'work')
		}
	);
    //Cette fonction doit faire partie des méthodes du modèle, elle reçoit l'objet `db` en paramètre et déclare d'éventuelles relations entre ce modèle et d'autres modèles définis dans `db`.
	Mail.associate = (db) => {
		Mail.belongsTo(db.Person);
	};
	
	return Mail;

};