//`DataTypes` qui contient l'ensemble des types de données que l'on peut utiliser pour définir les champs des entités
module.exports = function (sequelize, DataTypes) {

	const Phone = sequelize.define(
		'Phone',
		{
			address: DataTypes.STRING,
      //type est ou bien 'home' ou 'work'
      type: DataTypes.ENUM('home', 'work')
		}
	);
    //Cette fonction doit faire partie des méthodes du modèle, elle reçoit l'objet `db` en paramètre et déclare d'éventuelles relations entre ce modèle et d'autres modèles définis dans `db`.
	Phone.associate = (db) => {
		Phone.belongsTo(db.Person);
	};
	
	return Phone;

};