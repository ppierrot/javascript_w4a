//on appel la libraire sequelize qui est l'ORM, il sevira de lien vers la bdd sans faire de requêtes sql dans le code mais en passant par des modèles
const Sequelize = require('sequelize');
//on appel body parser pour décrypter les données qu'on reçoit dans une requête
const bodyParser = require('body-parser');

//on connecte sequelize à notre base de donnée : nom bbd, user, password
const sequelize = new Sequelize('apiweb', 'apiweb', 'apiweb', {
  host: 'localhost',
  dialect: 'mysql',

  // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
  operatorsAliases: false
});

//on déclare une variable Person contenant un modèle (soit un tableau de bdd) de nom Person avec une colonne firstname et lastname
const Person = sequelize.define('Person', {
  firstname: Sequelize.STRING,//colonne de la table
  lastname: Sequelize.STRING//colonne de la table
});

//synchronisation avec la base de données
sequelize.sync();

//on déclare la libraire express et on le met dans la variable app 
const express = require('express')
const app = express(); //framework pour gérer le routage des liens URL

//pour automatiquement parser les données et les convertir en json quand on utilise app
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Gestion d'erreurs (Middleware)
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

//=======
//routage
//=======

//objet personCtrl contenant l'ensemble des fonctions qui font appel à l'entité Sequelize `Person`
var personCtrl = {

  //renvoie l'ensemble des personnes sous la forme d'un tableau d'objets en JSON
  get_all: (req, res) => {
    Person.findAll() //fonction asynchrone qui va choper toutes les infos de la BBD?
    .then((p)=>{ //tout va être enregistré dans le tableau p, renvoyé dans une promise
      res.json(p); //ce tableau je veux le transformer en json et en faire une requête http
    })
  },

  //ajoute une nouvelle entité `person` en base à partir des données
  create: (req, res) => {
    //on créé un objet constitué des données dans la requête
    Person.create({
        firstname: req.body.Firstname,
        lastname: req.body.Lastname
    //promise pour envoyer le résultat
    }).then(result =>
        res.send(result))
  },
  
  /*renvoi la personne en fonction de l'id en param*/
  get_by_id: (req, res) => {
    //on cherche par id avec la variable id dans la requête url puis on affiche les variables
    Person.findById(req.params.id, {
      attributes: ['id', 'Firstname', 'Lastname']
    }) // on renvoit le résultat avec une promise
    .then(persons => {
      res.send(persons);
    })
  },

  //modifie la personne d'identifiant id` à partir des données fournies dans la requête
  update_by_id: (req, res) => {
    //on va modifier en bdd les variables suivants avec les données contenu dans le body de la requête
    Person.update({
      firstname: req.body.Firstname,
      lastname: req.body.Lastname
    }, {
      returning: true,
      //on modifiera si l'id en base de données est égal à celui de la variable id de la requête
      where: {
          id: req.params.id
      }
    })
    //réponses
    .then(
      console.log('Person modified : \nID:' + req.params.id + '\nPrénom : ' + req.body.Firstname + '\nNom : ' + req.body.Lastname)
    )
    .returning(
      res.send('Person modified : \nID:' + req.params.id + '\nPrénom : ' + req.body.Firstname + '\nNom : ' + req.body.Lastname)
    )
    .error(
      console.log('The ID ' + req.params.id + 'does not exists...')
    )
    .catch(
      console.log('The ID ' + req.params.id + 'does not exists...')
    )
  },

  //supprime la personne d'identifiant id de la base de données
  delete_by_id: (req, res)=>{
    Person.destroy({
      //on supprimera si l'id en base de données est égal à celui de la variable id de la requête
      where: {
          id: req.params.id
      }
    })
    //réponse
    .then(
      res.send('The ID ' + req.params.id + ' has been deleted...')
    )
  },

}

//Routes
app.get('/', (req, res) => res.send('Hello World!'))

app.get('/person', personCtrl.get_all);

app.post('/person', personCtrl.create);

app.get('/person/:id', personCtrl.get_by_id);

app.put('/person/:id', personCtrl.update_by_id);

app.delete('/person/:id', personCtrl.delete_by_id);

//si on est connécté avec succès sur le port 3000, affiche le log suivant
app.listen(3000, () => console.log('Example app listening on port 3000!'))